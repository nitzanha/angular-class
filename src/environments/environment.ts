// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyCh1wOQb_vZ_AQ_y_jYfOujSpBWBaXXgp0",
    authDomain: "messages-3b1fc.firebaseapp.com",
    databaseURL: "https://messages-3b1fc.firebaseio.com",
    projectId: "messages-3b1fc",
    storageBucket: "messages-3b1fc.appspot.com",
    messagingSenderId: "72741627744"
  }
};
