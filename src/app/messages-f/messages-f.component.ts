import { MessagesService } from './../messages/messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'messages-f',
  templateUrl: './messages-f.component.html',
  styleUrls: ['./messages-f.component.css']
})
export class MessagesFComponent implements OnInit {
  messages;
  constructor(private service:MessagesService) { }

  ngOnInit() {
    this.service.getMessagesFire().subscribe(response=>{
      console.log(response);
      this.messages = response;
    })
  }

}
