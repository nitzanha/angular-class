import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import {AngularFireDatabase} from 'angularfire2/database';
import {environment } from './../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class MessagesService {
  http:Http;
  getMessages(){
  //return ['Message1','Message2','Message3','Message4'];
  //get messages from the slim rest api (Don't say DB)
  let token = localStorage.getItem('token');
  let options = {
    headers: new Headers({
      'Authorization':'Bearer '+token
    })
  }
  return this.http.get(environment.url + 'messages',options);
  }

  getMessagesFire(){
    return this.db.list('/messages').valueChanges();
  }

  getMessage(id){
    return this.http.get(environment.url + 'messages/'+id);
  }

  postMessage(data){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);
    return this.http.post(environment.url + 'messages',
      params.toString(),
      options);

  }

  putMessage(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);
    return this.http.put(environment.url + 'messages/'+ key,params.toString(), options);
  }


   deleteMessage(key){ 
    return this.http.delete(environment.url + 'messages/'+key);
  }


  login(credentials){ 
   let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
    return this.http.post(environment.url + 'auth',params.toString(),options).map(response=>{
      let token = response.json().token;
      if(token) localStorage.setItem('token',token);
      console.log(token);
    });
  }


  constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
   }

}
