//import { Component, OnInit } from '@angular/core';
import { UsersService } from './../users.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {

@Output() addUser: EventEmitter<any> = new EventEmitter<any>();
@Output() addUserPs: EventEmitter<any> = new EventEmitter<any>();

  service:UsersService;

  userform = new FormGroup({
    user:new FormControl(),
  });

  sendData(){
    this.addUser.emit(this.userform.value.user);
    console.log(this.userform.value);
    this.service.postUser(this.userform.value).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
      }
    )
  };

  constructor(service:UsersService) {
    this.service=service;
   }

  ngOnInit() {
  }

}


