import { UsersService } from './users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  //messages = ['Message1','Message2', 'Message3','Message4'];
  users;
  usersKeys;

  constructor(service:UsersService) {
    service.getUsers().subscribe(response=>{
        //console.log(response.json());
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);
      });

   }

  ngOnInit() {
  }

} 




