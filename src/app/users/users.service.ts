import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class UsersService {
  http:Http;
  getUsers(){
  //return ['Message1','Message2','Message3','Message4'];
  //get messages from the slim rest api (Don't say DB)
  return this.http.get('http://localhost/angular/slim/users');
  }

    geUser(id){
    return this.http.get('http://localhost/angular/slim/users/'+id);
  }

  postUser(data){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('user',data.user);
    return this.http.post('http://localhost/angular/slim/users',
      params.toString(), options);

  }


   deleteUser(key){ 
    return this.http.delete('http://localhost/angular/slim/users/'+key);
  }

  constructor(http:Http) {
    this.http = http;
   }

}
